from paramiko import SSHClient
import paramiko
from scp import SCPClient
import time

ssh = SSHClient()
#ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
#ssh.connect('taurus.fis.agh.edu.pl', username='6mikulec', password='', key_filename='./ssh/id_rsa')
#ssh.connect('taurus.fis.agh.edu.pl', username='6mikulec', port=22,password='')
ssh.connect('10.42.0.36', username='pi', port=22, password='raspberry')

# SCPCLient takes a paramiko transport as an argument
scp = SCPClient(ssh.get_transport())

#start= time.time()
#
#while(time.time() - start < 5):
#    stdin, stdout, stderr = ssh.exec_command('cat ~/Desktop/proj/data.txt')
#    print(str(stdout.read(), "utf-8"), flush=True)

stdin, stdout, stderr = ssh.exec_command('cat /mnt/ramdisk/data.txt')
print(str(stdout.read(), "utf-8"), flush=True)
#    stdin, stdout, stderr = ssh.exec_command('python ~/Desktop/proj/skrypt.py')    
#get or put file over ssh
#scp.put('test.txt', 'test2.txt')
#scp.get('~/Desktop/proj/data.txt')


# Uploading the 'test' directory with its content in the
# '/home/user/dump' remote directory
#scp.put('test', recursive=True, remote_path='/home/user/dump')
    
scp.close()
ssh.close()
