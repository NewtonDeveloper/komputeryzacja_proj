import RPi.GPIO as GPIO
#from EmulatorGUI import GPIO #import RPi.GPIO as GPIO import time import traceback
import time 
import random

def led_on_random_time():
    # milisecons
    result = 0
    while(result < 0.1 or result > 0.5):
        result = random.random()*0.5
    return result
    

def time_space_between():
    result = 0
    while(result < 1 or result > 3):
        result = random.random()*3
        print(result)
    time.sleep(result)
    

GPIO.setmode(GPIO.BCM)

#led 
GPIO.setup(20, GPIO.OUT)
GPIO.output(20,GPIO.HIGH)

#button high voltage
GPIO.setup(21, GPIO.OUT)
GPIO.output(21,GPIO.HIGH)
GPIO.setup(16, GPIO.OUT)
GPIO.output(16,GPIO.HIGH)

# buttons
GPIO.setup(26, GPIO.IN)
GPIO.setup(19, GPIO.IN)

GPIO.input(26)
GPIO.input(19)

player_19 = 0
player_26 = 0
licznik = 0


#zbocze
#GPIO.add_event_detect(19, GPIO.RISING)
#GPIO.add_event_detect(26, GPIO.RISING)
#if GPIO.event_detected(channel):
#    print('Button pressed')


total_start_time = time.time()

while(player_19 + player_26 <= 10 and licznik < 7):
 

    GPIO.output(20,GPIO.HIGH)
    time_to_wait = led_on_random_time()
    start_time = time.time()
    end_time = time.time()
    #print (time_to_wait)

    flag = 0
    while( time_to_wait > end_time - start_time):
        end_time = time.time() 
       # print (end_time - start_time)
        if (flag != 0):
            continue
        if( GPIO.input(19) == 0 ):
            flag = 19
            player_19 += 1
            print("{0} {1} {2}".format(player_19,player_26,
                                time.time() - total_start_time))

            file_fd = open("/mnt/ramdisk/data.txt", "w")
            file_fd.write("{0} {1} {2}".format(player_19,player_26,time.time() - total_start_time))
            file_fd.close()



        elif( GPIO.input(26) == 0 ):
            flag = 26
            player_26 += 1
            print("{0} {1} {2}".format(player_19,player_26,
                                time.time() - total_start_time))

            file_fd = open("/mnt/ramdisk/data.txt", "w")
            file_fd.write("{0} {1} {2}".format(player_19,player_26,time.time() - total_start_time))
            file_fd.close() 
        

    GPIO.output(20, GPIO.LOW)
    time_space_between()
    licznik += 1

print ("player_19 {0}".format(player_19))
print ("player_26  {0}".format(player_26)) 

GPIO.cleanup()

